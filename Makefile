CC=g++

CFLAGS=-c -Wall -lsfml-graphics -lsfml-window -lsfml-system

all:
	g++ -c main.cpp square.cpp drawer.cpp rectangle.cpp circle.cpp
	g++ -o main main.o square.o drawer.o rectangle.o circle.o -lsfml-graphics -lsfml-window -lsfml-system

clean:
	rm -rf main
