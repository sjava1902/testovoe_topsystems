#include "square.h"

Square::Square(double a)
    : _sideLength(a)
{

}

double Square::shape() const{
    return _sideLength*_sideLength;
}

double Square::perimetr() const{
    return 4*_sideLength;
}

double Square::size() const{
    return _sideLength;
}