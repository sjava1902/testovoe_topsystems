
#ifndef IFIGURE_H
#define IFIGURE_H

class IFigure{
    virtual double shape() const = 0;
    virtual double perimetr() const = 0;
};

#endif