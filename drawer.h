
#ifndef DRAWER_H
#define DRAWER_H
#include <SFML/Graphics.hpp>
#include "figure.h"
#include "square.h"
#include "rectangle.h"
#include "circle.h"

class Drawer{
public:
    Drawer();
    void draw(Square*);
    void draw(Rectangle*);
    void draw(Circle*);
    void display();
private:
    sf::RenderWindow *_window;
    std::vector<Square*> _squares;
    std::vector<Rectangle*> _rectangles;
    std::vector<Circle*> _circles;
};

#endif