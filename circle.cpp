#include "circle.h"
#include "math.h"

Circle::Circle(double r)
    : _radius(r)
{

}

double Circle::perimetr() const{
    return 2 * M_PI * _radius;
}

double Circle::shape() const{
    return M_PI * _radius * _radius;
}

double Circle::radius() const{
    return _radius;
}